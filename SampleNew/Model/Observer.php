<?php
class Webapp_Smsalert_Model_Observer
{

			public function newOrder(Varien_Event_Observer $observer)
			{
				$order = $observer->getEvent()->getOrder();
				$ApiUrl = Mage::getStoreConfig('sms_alert_section/sms_alert_group/api_url');
				$SenderId = Mage::getStoreConfig('sms_alert_section/sms_alert_group/sender_id');
				$UserName = Mage::getStoreConfig('sms_alert_section/sms_alert_group/uname');
				$Pass = Mage::getStoreConfig('sms_alert_section/sms_alert_group/pass');
				$mobile =  $order->getBillingAddress()->getTelephone();
				$incrementId = $order->getIncrementId();
				$model = Mage::getModel("smsalert/sms")->load(1);
				
				$msg = str_replace("#OrderId",$incrementId,$model->getMessage());
				$msg = str_replace("#Price",$order->getGrandTotal(),$msg);
     
				Mage::log($ApiUrl.'?uname='.$UserName .'&pass='.$Pass.'&send='.$SenderId.'&dest='.$mobile.'&msg='.$msg,null,'newOrder.log');
				
				if(trim($ApiUrl) and trim($SenderId) and trim($UserName) and trim($Pass))
				{
					$query_string = 'uname='.$UserName .'&pass='.$Pass.'&send='.$SenderId.'&dest='.$mobile.'&msg='.$msg;
					
					$result = $this->curlUsingGet($ApiUrl,$query_string);
					
					
				}

			}
		
			public function updateOrderStatus(Varien_Event_Observer $observer)
			{
	            $post = Mage::app()->getRequest()->getPost('history');
				
				
				
				if(isset($post['is_customer_notified']) and $post['is_customer_notified'])	
				{			
				
				$order = $observer->getEvent()->getOrder();	
				$ApiUrl = Mage::getStoreConfig('sms_alert_section/sms_alert_group/api_url');
				$SenderId = Mage::getStoreConfig('sms_alert_section/sms_alert_group/sender_id');
				$UserName = Mage::getStoreConfig('sms_alert_section/sms_alert_group/uname');
				$Pass = Mage::getStoreConfig('sms_alert_section/sms_alert_group/pass');
				$mobile =  $order->getBillingAddress()->getTelephone();
				$incrementId = $order->getIncrementId();
				
	
			
				
				$order_status = $order->getStatus();
				
				$template_id = false;
				
				
								
				if($order_status=='canceled')
				{
					$template_id = 3;
				
				}
				if($order_status=='cancelled_by_admin')
				{
					$template_id = 12;
					
				
				}
				else if($order_status=='processing' || $order_status=='order_shipped')
				{
				    $template_id = 4;	
				}
				else if($order_status=='pending' )
				{
					$template_id = 5;
				}
				else if($order_status=='pending_payment')
				{
					$template_id = 6;
				}
				else if($order_status=='closed')
				{
					$template_id = 7;
				}
				
				else if($order_status=='holded' || $order_status=='on_hold')
				{
					$template_id = 8;
				}
				else if($order_status=='complete' || $order_status=='order_delivered')
				{
					$template_id = 9;
				}
				else if($order_status=='manufacturing_wys' || $order_status=='manufacturing')
				{
					$template_id = 10;
				}
				else if($order_status=='manufacturing_verfication_pendin')
				{
					$template_id = 11;
				}
				
				
				
				if($template_id)
				{
				$model = Mage::getModel("smsalert/sms")->load($template_id);
					if($model->getStatus())
					{
					$msg = $model->getMessage();
					if($template_id==12)
					$msg = str_replace("#Price",$order->getGrandTotal(),$msg);	
					$msg = str_replace("#OrderId",$incrementId,$msg);
					
						if(trim($ApiUrl) and trim($SenderId) and trim($UserName) and trim($Pass))
						{
							$query_string = 'uname='.$UserName .'&pass='.$Pass.'&send='.$SenderId.'&dest='.$mobile.'&msg='.$msg;
							
						
							
							$result = $this->curlUsingGet($ApiUrl,$query_string);
							
							
						}
						
					}
				
				}
				
				}
				
 
			}
			
			
function curlUsingGet($url,$query_string)
    {

  $Curl_Session = curl_init($url);
  curl_setopt ($Curl_Session, CURLOPT_POST, 1);
  curl_setopt ($Curl_Session, CURLOPT_POSTFIELDS, $query_string);
  curl_setopt ($Curl_Session, CURLOPT_FOLLOWLOCATION, 1);
  curl_setopt($Curl_Session, CURLOPT_RETURNTRANSFER,1);
  $result=curl_exec ($Curl_Session);
  return $result;
}
			
		
}
