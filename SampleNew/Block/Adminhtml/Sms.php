<?php


class Webapp_Smsalert_Block_Adminhtml_Sms extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_sms";
	$this->_blockGroup = "smsalert";
	$this->_headerText = Mage::helper("smsalert")->__("Sms Manager");
	$this->_addButtonLabel = Mage::helper("smsalert")->__("Add New Item");
	parent::__construct();
	
	}

}