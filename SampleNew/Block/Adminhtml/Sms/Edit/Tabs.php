<?php
class Webapp_Smsalert_Block_Adminhtml_Sms_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("sms_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("smsalert")->__("Item Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("smsalert")->__("Item Information"),
				"title" => Mage::helper("smsalert")->__("Item Information"),
				"content" => $this->getLayout()->createBlock("smsalert/adminhtml_sms_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
