<?php
class Webapp_Smsalert_Block_Adminhtml_Sms_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("smsalert_form", array("legend"=>Mage::helper("smsalert")->__("Item information")));

				
						$fieldset->addField("title", "text", array(
						"label" => Mage::helper("smsalert")->__("Title"),					
						"class" => "required-entry",
						"required" => true,
						"name" => "title",
						));
						
						
						
						$fieldset->addField("message", "text", array(
						"label" => Mage::helper("smsalert")->__("SMS TEXT"),					
						"class" => "required-entry",
						"required" => true,
						"name" => "message",
						));
						
					
									
						 $fieldset->addField('status', 'select', array(
						'label'     => Mage::helper('smsalert')->__('Status'),
						'values'   => Webapp_Smsalert_Block_Adminhtml_Sms_Grid::getValueArray2(),
						'name' => 'status',
						));
						
						
						

				if (Mage::getSingleton("adminhtml/session")->getSmsData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getSmsData());
					Mage::getSingleton("adminhtml/session")->setSmsData(null);
				} 
				elseif(Mage::registry("sms_data")) {
				    $form->setValues(Mage::registry("sms_data")->getData());
				}
				return parent::_prepareForm();
		}
}
