<?php

class Webapp_Smsalert_Adminhtml_SmsController extends Mage_Adminhtml_Controller_Action
{
		protected function _initAction()
		{
				$this->loadLayout()->_setActiveMenu("smsalert/sms")->_addBreadcrumb(Mage::helper("adminhtml")->__("Sms  Manager"),Mage::helper("adminhtml")->__("Sms Manager"));
				return $this;
		}
		public function indexAction() 
		{
			    $this->_title($this->__("Smsalert"));
			    $this->_title($this->__("Manager Sms"));

				$this->_initAction();
				$this->renderLayout();
		}
		public function editAction()
		{			    
			    $this->_title($this->__("Smsalert"));
				$this->_title($this->__("Sms"));
			    $this->_title($this->__("Edit Item"));
				
				$id = $this->getRequest()->getParam("id");
				$model = Mage::getModel("smsalert/sms")->load($id);
				if ($model->getId()) {
					Mage::register("sms_data", $model);
					$this->loadLayout();
					$this->_setActiveMenu("smsalert/sms");
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Sms Manager"), Mage::helper("adminhtml")->__("Sms Manager"));
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Sms Description"), Mage::helper("adminhtml")->__("Sms Description"));
					$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
					$this->_addContent($this->getLayout()->createBlock("smsalert/adminhtml_sms_edit"))->_addLeft($this->getLayout()->createBlock("smsalert/adminhtml_sms_edit_tabs"));
					$this->renderLayout();
				} 
				else {
					Mage::getSingleton("adminhtml/session")->addError(Mage::helper("smsalert")->__("Item does not exist."));
					$this->_redirect("*/*/");
				}
		}

		public function newAction()
		{

		$this->_title($this->__("Smsalert"));
		$this->_title($this->__("Sms"));
		$this->_title($this->__("New Item"));

        $id   = $this->getRequest()->getParam("id");
		$model  = Mage::getModel("smsalert/sms")->load($id);

		$data = Mage::getSingleton("adminhtml/session")->getFormData(true);
		if (!empty($data)) {
			$model->setData($data);
		}

		Mage::register("sms_data", $model);

		$this->loadLayout();
		$this->_setActiveMenu("smsalert/sms");

		$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Sms Manager"), Mage::helper("adminhtml")->__("Sms Manager"));
		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Sms Description"), Mage::helper("adminhtml")->__("Sms Description"));


		$this->_addContent($this->getLayout()->createBlock("smsalert/adminhtml_sms_edit"))->_addLeft($this->getLayout()->createBlock("smsalert/adminhtml_sms_edit_tabs"));

		$this->renderLayout();

		}
		public function saveAction()
		{

			$post_data=$this->getRequest()->getPost();


				if ($post_data) {

					try {


						$model = Mage::getModel("smsalert/sms");
						$model->addData($post_data)
						->setId($this->getRequest()->getParam("id"))
						->save();
						
						$resource = Mage::getSingleton('core/resource');
						$writeConnection = $resource->getConnection('core_write');
						$table = $resource->getTableName('sms_template');
						$query = "UPDATE {$table} SET message = '{$model->getMessage()}' WHERE id = ". $model->getId();
						$writeConnection->query($query);
     
    /**
     * Execute the query
     */
    $writeConnection->query($query);

						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Sms template was successfully saved"));
						Mage::getSingleton("adminhtml/session")->setSmsData(false);

						if ($this->getRequest()->getParam("back")) {
							$this->_redirect("*/*/edit", array("id" => $model->getId()));
							return;
						}
						$this->_redirect("*/*/");
						return;
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						Mage::getSingleton("adminhtml/session")->setSmsData($this->getRequest()->getPost());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					return;
					}

				}
				$this->_redirect("*/*/");
		}



		public function deleteAction()
		{
				if( $this->getRequest()->getParam("id") > 0 ) {
					try {
						$model = Mage::getModel("smsalert/sms");
						$model->setId($this->getRequest()->getParam("id"))->delete();
						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
						$this->_redirect("*/*/");
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					}
				}
				$this->_redirect("*/*/");
		}

		
}
