<?php
/**
 * Created by PhpStorm.
 * User: hulk
 * Date: 21/8/14
 * Time: 4:33 PM
 */

class Webappmate_DynamicPricing_Model_Observer
{
    /**
     * Evaluate and assign the price of each jewellery item added to Quote
     * @param Varien_Event_Observer $observer
     */
    public function computeFinalPriceForQuoteItem(Varien_Event_Observer $observer)
    {
        /** @var Webappmate_DynamicPricing_Helper_Data $priceHelper */
        $priceHelper = Mage::helper('dynamicpricing'); // price helper

        /** @var Mage_Sales_Model_Quote_Item[] $items */

        $items = $observer->getEvent()->getItems();

        foreach ($items as $item)
        {
            // computin dynamic price
            $dynamicPrice = $priceHelper->calculatePriceByQuoteItem($item);

            if ($dynamicPrice)
            {
                // apply catalog rule price to final price
                $rulePrice = Mage::getModel('catalogrule/rule')->calcProductPriceRule($item->getProduct(), $dynamicPrice);
                $dynamicSpecialPrice =$dynamicPrice;
                $specialPrice = $item->getProduct()->getSpecialPrice();
                if($specialPrice!=null)
                {
                    $regularPrice = $item->getProduct()->getPrice();
                    $discountFactor = ($regularPrice - $specialPrice)/$regularPrice;
                    $dynamicSpecialPrice = $dynamicPrice * ( 1 - $discountFactor );
                }
                if ($rulePrice!=null)
                {
                    $item->setCustomPrice(min($rulePrice,$dynamicSpecialPrice));
                    $item->setOriginalCustomPrice(min($rulePrice,$dynamicSpecialPrice));
                }
                else
                {
                    $item->setCustomPrice($dynamicSpecialPrice);
                    $item->setOriginalCustomPrice($dynamicSpecialPrice);
                }

                $item->setCustomPriceSeted(true);
            }
        }
    }
}