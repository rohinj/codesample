<?php

/**
 * Class Webappmate_DynamicPricing_Helper_Data
 *
 *  // Rename: Webappmate_DynamicPricing_Helper_Price
 *
 * Service to calculate final price of a product / jewellery item
 *
 */
class Webappmate_DynamicPricing_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Calculates price for a jewellery item
     *
     * Supply configuration in following format:
     *
     * configuration: [
     *      sku : required / string
     *      productId: required / integer
     *      colorClarity: string
     *      ringSize: integer / required for rings only
     *      makingCharge: string
     *      otherStonePrice: string
     *      metal: [
     *          color: string
     *          purity: string
     *      ]
     *      productMarkup
     * ]
     *
     * @param array $configuration
     * @return float
     */
    public function calculatePrice(array $configuration)
    {

        $price = $this->_computePrice($configuration);

        return $price;
    }

    /**
     * Get the Price of a Quote Item
     *
     * @param Mage_Sales_Model_Quote_Item $item
     * @return float
     */
    public function calculatePriceByQuoteItem(Mage_Sales_Model_Quote_Item $item)
    {
        // Write all logic to convert quote item to above configuration

        $configuration = array();
        if($item->getProduct()->getTypeId()!='simple')
        {
            return null;
        }
        else
        {
            $selectedOptions = $item->getProduct()->getTypeInstance(true)->getOrderOptions($item->getProduct());
            $product = Mage::getModel('catalog/product')->load($item->getProductId());
            if(!isset($selectedOptions['options']))
            {
                return null;
            }
            else
            {
                foreach($selectedOptions['options'] as $option) {
                    switch($option['label']){
                        case 'Diamond Type':
                            $configuration['colorClarity'] = $option['value'];
                            break;
                        case 'Carat':
                            $configuration['metal']['purity'] = $option['value'];
                            break;
                        case 'Metal':
                            $configuration['metal']['color'] = $option['value'];
                            break;
                        case 'Size':
                            $configuration['ringSize'] = $option['value'];
                            break;
                    }
                }
                $configuration['metal']['weight'] = $product->getData('metal_weight');
                $configuration['productMarkup'] = $product->getData('product_markup');
                $configuration['makingCharge'] = $product->getData('making_charge');
                $configuration['otherStonePrice'] = $product->getData('other_stone_price');
                $configuration['sku'] = $product->getSku();
                $configuration['productId'] = $product->getId();
                $configuration = $this->getDiamondInfo($configuration,$product->getId());
                return $this->calculatePrice($configuration);
            }
        }
    }

// get the dimanod Info 

    public function getDiamondInfo(array $configuration,$productId)
    {
        $product = Mage::getModel('catalog/product')->load($productId);
        $allDiamondInfo = array();
        for($i = 1;$i <21 ;$i++)
        {
            $shape = $product->getAttributeText('diamond'.$i.'_shape');
            $piece = $product->getData('diamond'.$i.'_no_pieces');
            $totalWeight = $product->getData('diamond'.$i.'_carat_wt');
            if(isset($shape) && isset($piece) && isset($totalWeight))
            {
                $diamondInfo =array(
                    "shape" => $shape,
                    "piece" => $piece,
                    "total_weight" => $totalWeight,
                );
                $allDiamondInfo[] = $diamondInfo;
            }
        }
        $configuration['stones'] = base64_encode(serialize($allDiamondInfo));
        return $configuration;
    }

    protected function _computePrice(array $configuration)
    {
        // Use PCJ's web service to get the price

        $responseData = $this->_queryPCJService($configuration);


        return (float) $responseData['price'];
    }

// get the Product data & calculate price

    public function getFinalDynamicData(array $configuration)
    {
        $dynamicData = $this->_queryPCJService($configuration);
        $dynamicSpecialPrice = $dynamicData['price'];
        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $configuration['sku']);
        $specialPrice = $product->getSpecialPrice();
        if($specialPrice!=null)
        {
           $regularPrice = $product->getPrice();
           $discountFactor = ($regularPrice - $specialPrice)/$regularPrice;
           $dynamicSpecialPrice = $dynamicData['price'] * ( 1 - $discountFactor );
        }
        $rulePrice = Mage::getModel('catalogrule/rule')->calcProductPriceRule($product, $dynamicData['price']);
        if($rulePrice!=null)
        {
            $dynamicData['newPrice'] = min($rulePrice,$dynamicSpecialPrice);
        }
        elseif($dynamicSpecialPrice < $dynamicData['price'])
        {
            $dynamicData['newPrice'] = $dynamicSpecialPrice;
        }
        return $dynamicData;
    }

// return the data using webserivce

    protected function _queryPCJService(array $configuration)
    {
        $apiUrl = (string)Mage::getConfig()->getNode('global/price_calculation_api/url');
   

        $httpClient = new Zend_Http_Client($apiUrl, array(
            'adapter' => 'Zend_Http_Client_Adapter_Curl'
        ));

        // caution: maybe you've to set the headers Content-Type:application/x-www-form-urlencoded
        $httpClient->setHeaders('Content-Type', 'application/x-www-form-urlencoded');
        Mage::log( $configuration,null,'pcj_price.log');

        $configuration = array(
            'product_id' => $configuration['productId'] ,
            'sku' => $configuration['sku'] ,
            'ring_size' => (isset($configuration['ringSize']) ? $configuration['ringSize'] : ""),
            'metal_purity' => $configuration['metal']['purity'],
            'metal_color' =>  $configuration['metal']['color'],
            'metal_weight' => $configuration['metal']['weight'],
            'diamond' => $configuration['colorClarity'],
            'making_charge' => $configuration['makingCharge'],
            'product_markup' => $configuration['productMarkup'],
            'other_stone_price' => $configuration['otherStonePrice'],
            'stones' => $configuration['stones'],
        );
        
        $httpClient->setParameterPost($configuration);
        $response = $httpClient->request('POST');

        $resultData = json_decode($response->getBody());

        $result = array(
            'metal' => array(
                'weight' =>  $resultData->metal_weight,
                'price'  => $resultData->metal_price,
            ),
            'stonePrice' => $resultData->stone_price,
            'makingCharge' => $resultData->making_charge,
            'vat' => $resultData->vat,
            'price' => $resultData->price,
            'sku' => $resultData->sku,
        );

        return $result;
    }
}