<?php

class Webappmate_DynamicPricing_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        echo "index controller for dynamic pricing";
    }

// get the Params & return the updated price according to the params

    public function dataAction()
    {
        $frontendHelper = Mage::helper('dynamicpricing');
        $params =$this->getRequest()->getParams();
        $validator = array("sku","productId","colorClarity","makingCharge","otherStonePrice","productMarkup","stones");
        $validatorMetal = array("purity","weight","color");
        $paramsData = json_decode($params['params'],true);
        $configuration = array();
        foreach($paramsData as $key => $value)
        {
            if($key=="metal")
            {
                foreach($value as $i => $j)
                {
                   if(in_array($i,$validatorMetal)==false)
                   {
                       return $this->getResponse()->setHttpResponseCode(400);
                   }
                }
            }
            elseif(in_array($key,$validator)==false && $key!="ringSize")
            {
                //var_dump($key." ".$value);
                return $this->getResponse()->setBody(json_encode(array("status" => "error")));
            }
        }
        $configuration=$paramsData;

        if(isset($paramsData['ringSize']))
        {
            $configuration["ringSize"] =$paramsData['ringSize'];
        }
        $frontendDynamicData = $frontendHelper->getFinalDynamicData($configuration);
        $response = $frontendDynamicData;
        return $this->getResponse()->setBody(json_encode($response))->setHttpResponseCode(200)->setHeader('Content-type', 'application/json');
    }
}